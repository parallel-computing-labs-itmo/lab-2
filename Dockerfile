FROM ubuntu:20.04
LABEL name="spo"
COPY lab-2/main-2.c .
COPY lab-2/Makefile .
COPY lab-2/start.sh .
COPY lab-2/start_with_params.sh .
RUN apt update
RUN apt install -y --allow-unauthenticated \
    gcc \
    clang \
    cmake \
    make \