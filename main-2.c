#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>

//Параметры варианта
//A = 280
//map = 4/6
//merge = 4
//sort = 4
//N1 = 474 - 10мс
//N2 = 11543 - 5000 мс
//
// Стандартный файл
//𝑁 = 𝑁1, 𝑁1 + Δ, 𝑁1 + 2Δ, 𝑁1 + 3Δ, . . . , 𝑁2
//
//Для скриптов с кол ядер
//𝑁 = 𝑁1, 𝑁1 + Δ, 𝑁1 + 2Δ, 𝑁1 + 3Δ, . . . , 𝑁2
//
//
//значение Δ выбрать так: Δ = (𝑁2 − 𝑁1)/10 -> (11543-474)/10 = 1107
//
//
//Провести верификацию значения X. Добавить в конец цикла вывод значения X и изменить число экспериментов на 5. Сравнить значения X
//для распараллеленной программы и нераспараллеленной.

float random_float(float min, float max) {
    return min + ((float) rand() / (float) RAND_MAX) * (max - min);
}

void generateMess_1(float *mess, int n) {
    #ifdef _OPENMP
        #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
            #pragma omp parallel for default(none) shared(mess, n) schedule(SCHED_ALG, CHUNK_SIZE)
        #else
            #pragma omp parallel for default(none) shared(mess, n) schedule(auto)
        #endif
    #endif
    for (int i = 0; i < n; i++) {
        mess[i] = random_float(1.0, 280.0);
    }
}

void generateMess_2(float *mess, int n) {
    #ifdef _OPENMP
        #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
                #pragma omp parallel for default(none) shared(mess, n) schedule(SCHED_ALG, CHUNK_SIZE)
            #else
                #pragma omp parallel for default(none) shared(mess, n) schedule(auto)
            #endif
    #endif
    for (int i = 0; i < n; i++) {
        mess[i] = random_float(280.0, 280.0 * 10.0);
    }
}

void hyperbolic_cotangent_of_sqrt_mess(float *mess, int n) {
    #ifdef _OPENMP
        #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
                #pragma omp parallel for default(none) shared(mess, n) schedule(SCHED_ALG, CHUNK_SIZE)
            #else
                #pragma omp parallel for default(none) shared(mess, n) schedule(auto)
            #endif
    #endif
    for (int i = 0; i < n; i++) {
        mess[i] = 1.0 / tanh(sqrt(mess[i]));
    }
}

void sum__mess(float *mess, int n) {
    float oldElement = 0;
    #ifdef _OPENMP
        #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
                #pragma omp parallel for default(none) shared(mess, n, oldElement) schedule(SCHED_ALG, CHUNK_SIZE)
            #else
                #pragma omp parallel for default(none) shared(mess, n, oldElement) schedule(auto)
            #endif
    #endif
    for (int i = 0; i < n; i++) {
        float sum = mess[i] + oldElement;
        oldElement = mess[i];
        mess[i] = exp(log10(sum));
    }
}

void marge(float *mess1, float *mess2, int size) {
    #ifdef _OPENMP
        #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
                #pragma omp parallel for default(none) shared(mess1, mess2, size) schedule(SCHED_ALG, CHUNK_SIZE)
            #else
                #pragma omp parallel for default(none) shared(mess1, mess2, size) schedule(auto)
            #endif
    #endif
    for (int i = 0; i < size; i++) {
        mess2[i] = (mess1[i] > mess2[i]) ? mess1[i] : mess2[i];
    }
}

//void gnome_sort(float arr[], int size) {
//    int index = 0;
//    while (index < size) {
//        if (index == 0 || arr[index] >= arr[index - 1]) {
//            index++;
//        } else {
//            float temp = arr[index];
//            arr[index] = arr[index - 1];
//            arr[index - 1] = temp;
//            index--;
//        }
//    }
//}

void print(float *mess, int n) {
    printf("[");
    #ifdef _OPENMP
        #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
                #pragma omp parallel for default(none) shared(mess, n) schedule(SCHED_ALG, CHUNK_SIZE)
            #else
                #pragma omp parallel for default(none) shared(mess, n) schedule(auto)
            #endif
    #endif
    for (int i = 0; i < n - 1; i++) {
        printf("%f, ", mess[i]);
    }
    printf("%f", mess[n - 1]);
    printf("]\n");
}

float sum_of_sines(float *M2, int size) {
    // Находим минимальный ненулевой элемент массива M2
    int min_nonzero = -1;
    float sum = 0;
    #ifdef _OPENMP
        #if defined(CHUNK_SIZE) && defined(SCHED_ALG)
                #pragma omp parallel for default(none) shared(M2, size, min_nonzero, sum) schedule(SCHED_ALG, CHUNK_SIZE)
            #else
                #pragma omp parallel for default(none) shared(M2, size, min_nonzero, sum) schedule(auto)
            #endif
    #endif
    for (int i = 0; i < size; ++i) {
        if (M2[i] != 0 && (min_nonzero == -1 || M2[i] < M2[min_nonzero])) {
            min_nonzero = i;
        }
    }
    // Просматриваем элементы массива M2
    for (int i = 0; i < size; ++i) {
        // Проверяем, делится ли элемент на минимальный ненулевой элемент на четное число
        if ((int) M2[i] % (int) M2[min_nonzero] == 0) {
            if (((int) (M2[i] / M2[min_nonzero])) % 2 == 0) {
                // Если да, добавляем синус элемента к сумме
                sum += sin(M2[i]);
            }
        }
    }

    return sum;
}

int main(int argc, char *argv[]) {
    int i, N;
    float *mess_1;
    float *mess_2;
    struct timeval T1, T2;
    long delta_ms;
    N = atoi(argv[1]); // N равен первому параметру командной строки
    gettimeofday(&T1, NULL); // запомнить текущее время T1
    mess_1 = (float *) malloc(N * sizeof(float));
    mess_2 = (float *) malloc(N / 2 * sizeof(float));
    for (i = 0; i < 100; i++) { // 100 экспериментов
        srand(i); // инициализировать начальное значение ГСЧ
        generateMess_1(mess_1, N);
        generateMess_2(mess_2, N / 2);
        hyperbolic_cotangent_of_sqrt_mess(mess_1, N);
        sum__mess(mess_2, N / 2);
        marge(mess_1, mess_2, N / 2);
        //gnome_sort(mess_2, N / 2);
        sum_of_sines(mess_2, N / 2);
        //printf("i - %d => x = %f\n", i, x);
    }
    gettimeofday(&T2, NULL); // запомнить текущее время T2
    delta_ms = (T2.tv_sec - T1.tv_sec) * 1000 +
               (T2.tv_usec - T1.tv_usec) / 1000;
    printf("%ld\n", delta_ms);
    return 0;
}
