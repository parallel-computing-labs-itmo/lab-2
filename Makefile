no-omp:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -o lab2-no-omp main-2.c -lm

omp:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -o lab2-omp main-2.c -lm -fopenmp -lomp

omp-with-params-dynamic-2:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -DCHUNK_SIZE=2 -DSCHED_ALG=dynamic -o lab2-omp-params-dynamic-2 main-2.c -lm -fopenmp -lomp

omp-with-params-dynamic-4:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -DCHUNK_SIZE=4 -DSCHED_ALG=dynamic -o lab2-omp-params-dynamic-4 main-2.c -lm -fopenmp -lomp

omp-with-params-dynamic-6:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -DCHUNK_SIZE=6 -DSCHED_ALG=dynamic -o lab2-omp-params-dynamic-6 main-2.c -lm -fopenmp -lomp

omp-with-params-dynamic-8:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -DCHUNK_SIZE=8 -DSCHED_ALG=dynamic -o lab2-omp-params-dynamic-8 main-2.c -lm -fopenmp -lomp

omp-with-params-static-2:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -DCHUNK_SIZE=2 -DSCHED_ALG=static -o lab2-omp-params-static-2 main-2.c -lm -fopenmp -lomp

omp-with-params-static-4:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -DCHUNK_SIZE=4 -DSCHED_ALG=static -o lab2-omp-params-static-4 main-2.c -lm -fopenmp -lomp

omp-with-params-static-6:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -DCHUNK_SIZE=6 -DSCHED_ALG=static -o lab2-omp-params-static-6 main-2.c -lm -fopenmp -lomp

omp-with-params-static-8:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -DCHUNK_SIZE=8 -DSCHED_ALG=static -o lab2-omp-params-static-8 main-2.c -lm -fopenmp -lomp

omp-with-params-guided-2:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -DCHUNK_SIZE=2 -DSCHED_ALG=guided -o lab2-omp-params-guided-2 main-2.c -lm -fopenmp -lomp

omp-with-params-guided-4:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -DCHUNK_SIZE=4 -DSCHED_ALG=guided -o lab2-omp-params-guided-4 main-2.c -lm -fopenmp -lomp

omp-with-params-guided-6:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -DCHUNK_SIZE=6 -DSCHED_ALG=guided -o lab2-omp-params-guided-6 main-2.c -lm -fopenmp -lomp

omp-with-params-guided-8:
	clang -O3 -Ofast -ffinite-math-only -freciprocal-math -frounding-math -Wall -Werror -pedantic -DCHUNK_SIZE=8 -DSCHED_ALG=guided -o lab2-omp-params-guided-8 main-2.c -lm -fopenmp -lomp

build-lab: no-omp omp omp-with-params-dynamic-2 omp-with-params-dynamic-4 omp-with-params-dynamic-6 \
omp-with-params-dynamic-8 omp-with-params-static-2 omp-with-params-static-4 omp-with-params-static-6 \
omp-with-params-static-8 omp-with-params-guided-2 omp-with-params-guided-4 omp-with-params-guided-6 \
omp-with-params-guided-8