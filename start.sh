#!/bin/bash

echo 'no-omp:' > out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab2-no-omp $i >> out.txt
done
echo '' >> out.txt
echo 'omp:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab2-omp $i >> out.txt
done
echo '' >> out.txt
echo 'omp-params-guided-2:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab2-omp-params-guided-2 $i >> out.txt
done
echo '' >> out.txt
echo 'omp-params-guided-4:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab2-omp-params-guided-4 $i >> out.txt
done
echo '' >> out.txt
echo 'omp-params-guided-6:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab2-omp-params-guided-6 $i >> out.txt
done
echo '' >> out.txt
echo 'omp-params-guided-8:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab2-omp-params-guided-8 $i >> out.txt
done
