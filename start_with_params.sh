#!/bin/bash

echo '' >> out.txt
echo 'omp:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab2-omp $i >> out.txt
done

echo '' >> out.txt
echo 'omp-params-dynamic-8:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab2-omp-params-dynamic-8 $i >> out.txt
done

echo '' >> out.txt
echo 'omp-params-static-8:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab2-omp-params-static-8 $i >> out.txt
done

echo '' >> out.txt
echo 'omp-params-guided-8:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab2-omp-params-guided-8 $i >> out.txt
done
